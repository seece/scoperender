#include <cstdio>
#include "eks_util.h"

namespace {
long getFilesize(FILE *fp) {
    fseek(fp, 0L, SEEK_END);
	long sz = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	return sz;
}
}

int util::dumpArrayToDisk(char *data, int length, const char *output_path) {
    FILE *fp;
    fp = fopen(output_path, "wb");

	if (fp == NULL) {
		printf("Couldn't open file %s for writing!", output_path);
		fclose(fp);
		return 0;	// wrote 0 bytes
    }

	fwrite(data, sizeof(char), length,  fp);

	int written = ftell(fp);
	fclose(fp);

	return written;
}
