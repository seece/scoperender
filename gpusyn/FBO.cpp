#include "FBO.h"
#include "GLDebug.h"
#include "Logger.h"

using eng::logger;

static bool isDepthFormat(GLenum format)
{
	switch (format) {
		case GL_DEPTH_COMPONENT16: return true;
		case GL_DEPTH_COMPONENT24: return true;
		case GL_DEPTH_COMPONENT32: return true;
		case GL_DEPTH_COMPONENT32F: return true;
		case GL_DEPTH_COMPONENT: return true;
	};

	return false;
};

void FBO::setDrawBuffers(const Buffer buffers[], int count) {
	if (isDepthFormat(buffers[count-1].extFormat)) {
		// if there's only depth buffer we don't write any color information
		if (count == 1) {
			glDrawBuffer(GL_NONE);
			return;
		}

		glDrawBuffers(textureCount-1, drawBuffers); 
		logger->info("FBO: Set %d draw buffers", count-1);
	} else {
		glDrawBuffers(textureCount, drawBuffers); 
	}

	eng::gl::assertGlError("Couldn't set FBO draw buffers");
}

FBO::FBO(int width, int height, const Buffer buffers[], int count):
width(width), height(height), fboName(0), textures(nullptr), drawBuffers(nullptr),
textureCount(count)
{
	textures = new GLuint[textureCount];
	drawBuffers = new GLenum[textureCount]; // we actually use only textureCount-1 buffers if there's a depth buffer present

	glGenFramebuffers(1, &fboName);
	eng::gl::assertGlError("Couldn't generate FBO name");
	bind(GL_FRAMEBUFFER);	

	glGenTextures(textureCount, textures);
	eng::gl::assertGlError("Couldn't generate texture names");

	int num_color_attachments = -1; // increased by one before use

	for (int i=0;i<textureCount;i++) {
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		eng::gl::assertGlError("FBO: Couldn't bind texture");

		logger->debug("FBO Texture: 0x%X, %d x %d. Type: %s", 
			buffers[i].intFormat,
			buffers[i].width,
			buffers[i].height,
			isDepthFormat(buffers[i].intFormat) ? "DEPTH" : "COLOR"				
		);

		glTexImage2D(GL_TEXTURE_2D, 
			0, 
			buffers[i].intFormat, 
			buffers[i].width, 
			buffers[i].height, 
			0, 
			buffers[i].extFormat,
			buffers[i].extFormatType,    
			0);					

		eng::gl::assertGlError("Couldn't create FBO texture");

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		eng::gl::assertGlError("Couldn't set FBO texture parameters");

		GLenum attachment;

		if (isDepthFormat(buffers[i].intFormat)) {
			attachment = GL_DEPTH_ATTACHMENT;

			if (i < textureCount-1) {
				logger->error("Depth component not last in format list");
			}
		}  else {
			num_color_attachments++;
			attachment = GL_COLOR_ATTACHMENT0 + num_color_attachments;
			drawBuffers[i] = attachment;
		}

		glFramebufferTexture(GL_FRAMEBUFFER, attachment, textures[i], 0);
		eng::gl::assertGlError("Couldn't set FBO texture");
	}

	setDrawBuffers(buffers, count);
	
	GLenum bufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(bufferStatus != GL_FRAMEBUFFER_COMPLETE) {
		logger->error("Framebuffer not complete! Status: 0x%X", bufferStatus);		
	}

	eng::gl::assertGlError("Framebuffer object creation failed");
}

FBO::~FBO() 
{
	glDeleteTextures(textureCount, textures);
	glDeleteFramebuffers(1, &fboName);

	delete[] textures;
	delete[] drawBuffers;
}

void FBO::bind(GLenum target)
{
	glBindFramebuffer(target, fboName);
	eng::gl::assertGlError("Couldn't bind framebuffer");
}

void FBO::bindDefault() 
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLuint FBO::getTextureID(int index)
{
	if (index < 0 || index >= textureCount) {
		logger->error("Invalid texture index %d", index);
		return 0;
	}

	return textures[index];
}

void FBO::clear(GLbitfield mask) 
{
	glClear(mask);
}

void FBO::clearDepth()
{
	clear(GL_DEPTH_BUFFER_BIT);
}

void FBO::setViewport()
{
	glViewport(0, 0, width, height);
}