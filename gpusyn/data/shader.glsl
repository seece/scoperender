#define RATE (44100.0)
#define NOTEMAGIC (1.059460646483)
#define TAU (2.0*PI)
#define NOTEFREQ(pitch) (pow(NOTEMAGIC, pitch) * 440.0)
#define PI (3.14159265358979323846)
#define FBO_WIDTH (4096.0)
#define EVENT_MAX (64)

struct Event {
	int begin;
	int type;
	float data;
	float param;
};

uniform int framesRendered;
uniform int eventCount;
layout(std140) uniform eventList {
	Event events[EVENT_MAX];
};

#ifdef VERT_SHADER
in vec2 position;	
void main()
{
	gl_Position = vec4(position, 0.0, 1.0);
}
#endif

#ifdef FRAG_SHADER
//layout(location=0) out vec4 out_Color;
layout(location=0) out float out_Value;

void main(void) {
	float pos = float(framesRendered) + gl_FragCoord.x;
	float t = pos/RATE;
	
	for (int i=0;i<eventCount;i++) {
		
	}
	
	float pitch = 0.0 + events[0].data*0.0;	// just a test
	float volume = 0.4;
	float wave = sin(t*2.0*PI*NOTEFREQ(pitch)) * volume;
	//float wave = ((mod(pos/128.0, 2.0)-0.5)*1.0);
	
	out_Value = wave;
}
#endif