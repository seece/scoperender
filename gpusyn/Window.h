#pragma once
/* Manages an SDL window.
 * Creates an OpenGL 3.3 core profile context on creation.
 */
#include <SDL.h>
#include "Input.h"
#include <cstdint>

class Window {
	public:
	struct Settings {
		int width;
		int height;
		bool fullscreen;
		bool vsync;
	};

	explicit Window(const Window::Settings& windowsettings);
	~Window();

	Settings getSettings() const;
	void draw();
	void* getWindowHandle();

	private:
	void initSDL(const Window::Settings& windowsettings);
	void cleanupSDL();

	// Disallow copy constructor and assignment
	Window(const Window&);
	Window& operator=(const Window&);

	double getTime(); // inaccurate timer in seconds
	int startTime;
	Settings currentSettings;
	SDL_Window* mainWindow;
	SDL_GLContext mainContext;
	void* windowHandle;
};