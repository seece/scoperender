#include <cassert>
#include "opengl.h"
#include "eks_math.h"
#include "synth.h"
#include "logger.h"

using eng::logger;
using eng::fsys::File;
using synth::Event;

Synth::Synth() : 
	frames_rendered(0),
	eventListData(nullptr),
	temp_buffer(nullptr),
	shadersource("data/shader.glsl"),
	shader(shadersource.c_str())
{
	glDisable(GL_DEPTH_TEST);

	logger->debug("Initializing synth");	
	logger->debug("Shader status: %s", shader.isValid() ? "valid" : "invalid");
	shader.use();

	buffer_width = 4096;
	buffer_height = 1;

	FBO::Buffer buffers[] = {
		{GL_R32F, GL_RED, GL_FLOAT, buffer_width, buffer_height},
	};

	framebuffer = std::unique_ptr<FBO>(new FBO(buffer_width, buffer_height, buffers, 1));
	framebuffer->bind();

	temp_buffer = new float[buffer_height * buffer_width];

	glGenBuffers(1, &eventListUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, eventListUBO);
	glBufferData(GL_UNIFORM_BUFFER, EVENT_LIST_SIZE, NULL, GL_DYNAMIC_DRAW);

	eventListData = new uint8_t[EVENT_LIST_SIZE];
}

Synth::~Synth()
{
	delete[] eventListData;
	glDeleteBuffers(1, &eventListUBO);
	delete[] temp_buffer;
}

// Writes the updated events to events-parameter.
/*
void Synth::update(synth::eventbuffer_t& events, int samplecount)
{

}
*/

void Synth::render(SAMPLE_TYPE* buffer, int framecount)
{
	assert(framecount == buffer_width*buffer_height);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLuint framesRenderedLoc = shader.getUniformLocation("framesRendered");
	glUniform1i(framesRenderedLoc, frames_rendered);

	GLuint eventListLoc = shader.getUniformBlockIndex("eventList");
	if (eventListLoc == GL_INVALID_INDEX) {
		logger->error("Invalid eventList uniform block");
	}

	glBindBuffer(GL_UNIFORM_BUFFER, eventListUBO);
	glUniformBlockBinding(shader.getId(), eventListLoc, UNIFORM_BLOCK_EVENT_LIST);
	glBindBufferRange(GL_UNIFORM_BUFFER, UNIFORM_BLOCK_EVENT_LIST, eventListUBO, 0, EVENT_LIST_SIZE);


	// update event buffer data
	glBufferSubData(GL_UNIFORM_BUFFER, 0, EVENT_LIST_SIZE, (GLvoid*)eventListData);

	// draw a full framebuffer quad
	glBegin(GL_QUADS);             
	glVertex3f(-1.0f, 1.0f, 0.0f); 
	glVertex3f( 1.0f, 1.0f, 0.0f); 
	glVertex3f( 1.0f,-1.0f, 0.0f); 
	glVertex3f(-1.0f,-1.0f, 0.0f); 
	glEnd();                       

	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, buffer_width, buffer_height, GL_RED, GL_FLOAT, temp_buffer);

	for (int i=0;i<framecount;i++) {
		float sampl = temp_buffer[i];
		buffer[i*2] = sampl;
		buffer[i*2+1] = sampl;
	}

	frames_rendered += framecount;
}

int Synth::serializeEvents(synth::eventbuffer_t& events)
{
	int event_count = events.size();

	for (auto& e : events) {
		Event* ev = reinterpret_cast<Event*>(&eventListData[0 + sizeof(Event) * event_count]);	
		*ev = e;

		event_count++;
	}

	return event_count;
}