#include "LineSynth.h"
#include <cstdio>

#define PI (3.141592653589)

LineSynth::LineSynth(float rate)
	: rate(rate),
	frames_rendered(0),
	queuePos(-1),
	currentStep(-1),
	lineStepCount(0),
	lineDrawSpeed(20.0f)
{

}

LineSynth::~LineSynth()
{

}

/*
void LineSynth::update(synth::eventbuffer_t& events, int samplecount)
{
}
*/

void LineSynth::render(SAMPLE_TYPE* buffer, int framecount)
{
	int pos = 0; // position in audio frames

	while (lineQueue.size() > 0 || working) {
		if (!working) {
				{
					std::lock_guard<std::mutex> lock(lineQueueMutex);
					// ready for more work
					target = lineQueue.front();
					// never let the queue to run empty
					if (lineQueue.size() > 1) {
						lineQueue.pop();
					}
				}
			working = true;
			currentStep = 0;
			lineStepCount = (int)
				(
				lineDrawSpeed * sqrt(target.points[0].x * target.points[0].x + target.points[1].y  * target.points[1].y)
				); // *(1.0f - (target.points[0].z + target.points[1].z) / 600.0f); // fog
			if (lineStepCount < 1) lineStepCount = 1; 
		}

		SCOPE_Point& p1 = target.points[0];
		SCOPE_Point& p2 = target.points[1];

		float part = 1.0f / lineStepCount;
		float x, y;

		for (currentStep; currentStep < lineStepCount; currentStep++) {
			if (pos >= framecount) {
				// the buffer is full, break and finish this line on later buffer
				goto bail_out;
			}

			float progress = currentStep * part;
			float iprogress = (1.0f - progress);
			x = p1.x * iprogress + p2.x * progress;
			y = p1.y * iprogress + p2.y * progress;

			buffer[2 * pos + 0] = x;
			buffer[2 * pos + 1] = y; 

			pos++;
		}

		// this is only hit if we didn't run out of line to draw
		working = false;
		//printf("finished (%.2f, %.2f) -> (%.2f, %.2f), buffer size: %u\n", p1.x, p1.y, p2.x, p2.y, lineQueue.size());
	}

	// render zeros to the remaining buffer
	for (int i = pos; i < framecount; i++) {
		buffer[2 * i + 0] = 0.0f;
		buffer[2 * i + 1] = 0.0f;
	}

	bail_out:
	frames_rendered += framecount;
}