#pragma once
/* A File class that supports easy file reloading and C-style string
 * conversion.
 */

#include <stdint.h>
#include <string>
#include <vector>

namespace eng {
namespace fsys {
// Returns the basename of a file path, e.g. "game" in "files/game.exe"
std::string extractBasename(const std::string& path);

class File {
	public:
	File(const std::string& path);
	//File(const char * path);
	~File();
	const void print();

	// Reloads the file from disk 
	uint64_t reload();

	// Returns the file data as a NUL-terminated C-string 
	char* c_str();

	// Returns the basename of the file, e.g. "article" in "data/article.txt" 
	const std::string getBasename();

	// Checks if the current modified date is newer than the one saved on last reload.
	bool beenModified();

	std::string getFilePath();
	std::string getFullPath();

	char* data;
	uint32_t size;
	// Last modification date as a 64-bit unsigned integer.
	uint64_t getLastModified();

	private:
	void load(const char * path);
	void updateLastModified();

	char* stringdata;	/* contains the data as a C-string, default: NULL */
	uint64_t lastModified;
	std::string filepath;
	std::string fullpath;
};

}; // namespace fsys
}; // namespace eng
