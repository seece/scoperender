#include "Input.h"
#include "Logger.h"
using eng::logger;

Input::Input() : oldKeyboardState(nullptr), keyboardState(nullptr) 
{
	if (!SDL_WasInit(0)) {
		logger->error("SDL not initialized before using Input class.");
	}

	initKeyboard();
	logger->info("Input module initialized.");
};

Input::~Input() 
{
	if (oldKeyboardState) {
		delete[] oldKeyboardState;
	}


};

void Input::initKeyboard()
{
	keyboardState = SDL_GetKeyboardState(&keyCount);

	if (keyCount == 0)  {
		logger->error("Unable to fetch SDL key count");
		return;
	}

	oldKeyboardState = new uint8_t[keyCount];
};

bool Input::checkKeyValid(int key)
{
	if (!keyboardState) {
		logger->error("No keyboard state");
		return false;
	}

	if (key < 0 || key >= keyCount)  {
		logger->error("Trying to check invalid key code %d", key);
		return false;
	}

	return true;
}


bool Input::keyDown(int key)
{
	if (!checkKeyValid(key))
		return false;

	if (!oldKeyboardState) {
		logger->error("No old keyboard state");
		return false;
	}

	return keyboardState[key] == 1;
}

bool Input::keyHit(int key)
{
	if (!checkKeyValid(key))
		return false;

	return (oldKeyboardState[key] != keyboardState[key]) && !oldKeyboardState[key];
}

void Input::saveKeyboardState()
{
	if (!keyboardState)
		return;

	memcpy(oldKeyboardState, keyboardState, sizeof(Uint8)*keyCount);
}

void Input::processEvents()
{
	saveKeyboardState();
	SDL_PumpEvents();
	keyboardState = SDL_GetKeyboardState(&keyCount);
	oldMouse = mouse;
	mouse.state = SDL_GetMouseState(&mouse.pos.x, &mouse.pos.y);
}

Input::MousePosition Input::mousePos()
{
	return mouse.pos;
}

bool Input::mouseDown(int button)
{
	return (mouse.state & SDL_BUTTON(button)) != 0;
}

bool Input::mouseHit(int button)
{
	bool hit_last_time = ((oldMouse.state && SDL_BUTTON(button)) != 0);

	if (mouseDown(button) && !hit_last_time) {
		return true;
	}

	return false;
}
