#pragma once
/*
 * An OpenGL shader program. Contains both vertex and fragment shaders.
 */

#include <string>
#include "opengl.h"
#include "Logger.h"

class ShaderProgram {
	public:
		explicit ShaderProgram(const GLchar* shaderSource);
		ShaderProgram(const GLchar* vertexsource, const GLchar* fragmentsource);
		~ShaderProgram();
		void use();
		GLint getUniformLocation(const GLchar* name);
		GLint getUniformBlockIndex(const GLchar* name);
		bool isValid(); // Returns false if the shader compilation failed.
		GLuint getId();

	private:
		void createAndLink(GLuint vertexShader, GLuint fragmentShader);
		bool compileShaders(const char* vertexString, const char* fragmentString);

		static bool compileShaderSource(GLuint shaderId, const char* prefix);

		static const std::string versionString;
		static const std::string vertexDefine;
		static const std::string fragmentDefine;

		GLuint
			programId;

		bool vertexvalid;
		bool fragmentvalid;

};	

