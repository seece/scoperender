#pragma once
/* Includes OpenGL 3.3 headers and the GLload library for initialization. */

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <glload/gl_all.h>
#include <glload/gl_load.h>
