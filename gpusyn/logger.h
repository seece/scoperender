#pragma once
/* A logger class used for global application logging.
 * Includes multiple inputs for different log levels. 
 */
#include <cstdarg>
#include <memory>


class LogSink {
	public:
	LogSink(const char* filename);
	virtual ~LogSink();
	void info(const char* format, ...);
	void debug(const char* format, ...); // messages printed only when compiled in Debug-mode.
	void error(const char* format, ...);

	protected:
	void write(const char* format, va_list argptr, FILE* output = stderr, bool messagebox = false);
	void openConsoleWindow();
};

namespace eng {
	// Global logger object that will be instantiated by the main program.
	// Allocated on the heap since this allows us to control its lifetime
	// in a more fine-grained manner.
	extern std::unique_ptr<LogSink> logger;
}