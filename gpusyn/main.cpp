
#include "eks_math.h"
#include "eks_util.h"
#include "event.h"
#include "logger.h"
#include "LineSynth.h"
//#include "audio_asio.h"
#include "audio.h"
#include "Window.h"
#include "Input.h"
#include "opengl.h"
#include "ShaderProgram.h"
#include "Filesystem.h"
#include "FBO.h"
#include "utils.h"

#include "portaudio.h"

#include <cassert>
#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <vector>

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <memory>

using eng::logger;
using eng::fsys::File;

//#define GLSL(version, shader)  "#version " #version "\r\n" #shader

void drawDebugStuff() 
{
	glBegin(GL_LINES);
	glVertex3f(0.5, 0.0, 0.0f);
	glVertex3f(0.25, 0.0, 0.0f);
	glEnd();
}

void drawLine(float x1, float y1, float x2, float y2)
{
	glBegin(GL_LINES);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glEnd();
}

void drawLine(SCOPE_Point_t p1, SCOPE_Point_t p2)
{
	drawLine(p1.x, p1.y, p2.x, p2.y);
}

bool programEnded = false;
LineSynth linesynth(AUDIO_RATE);
int bufferUnderflowCount = 0;
bool recordingAudio = false;
std::vector<float> recordedAudioBuffer;

static int audioCallback(const void *inputBuffer, void *outputBuffer,
	unsigned long framesPerBuffer,
	const PaStreamCallbackTimeInfo* timeInfo,
	PaStreamCallbackFlags statusFlags,
	void *userData)
{
	if (programEnded) { return paComplete; }
	if (statusFlags & paOutputUnderflow) {
		bufferUnderflowCount++;
	}

	float *out = (float*)outputBuffer;
	unsigned int i;
	(void)inputBuffer; /* Prevent unused variable warning. */

	linesynth.render(out, framesPerBuffer);

	if (recordingAudio) {
		auto size = recordedAudioBuffer.size();
		recordedAudioBuffer.resize(size + framesPerBuffer * 2);
		memcpy(&recordedAudioBuffer[size], out, sizeof(float) * framesPerBuffer * 2);
	}

	return paContinue;
}

int main(int argc, char* argv[])
{
	showConsole();
	eng::logger = std::unique_ptr<LogSink>(new LogSink("log.txt"));
	logger->info("Creating window");
	Window::Settings settings = {512, 512, false, false};
	Window win(settings);
	Input input;
	logger->info("Initializing audio");

	/*
	audio_asio_init(win.getWindowHandle(), 44100.0);
	audio_asio_set_callback(renderTestAudio);
	audio_asio_start();
	*/

	PaError perr;
	if ((perr = Pa_Initialize()) != paNoError) {
		puts(Pa_GetErrorText(perr));
		assert(false && "coudln't init portaudio");
	}

	PaDeviceIndex deviceCount = Pa_GetDeviceCount();
	PaDeviceIndex asioDevice = -1;

	printf("%d PortAudio devices: \n", deviceCount);
	for (PaDeviceIndex i = 0; i < deviceCount; i++) {
		const PaDeviceInfo* info = Pa_GetDeviceInfo(i);
		if (!info) continue;
		printf("%i: ", i); 
		printf("%s, %d IN, %d OUT, API: %d\n", info->name != NULL ? info->name : "unnamed", 
			info->maxInputChannels, 
			info->maxOutputChannels,
			info->hostApi);

		//if (info->hostApi == 1 && strstr(info->name, "Easy USB")) {
		if (info->hostApi == 1 && strstr(info->name, "ASIO4ALL")) {
			asioDevice = i;
		}
	}

	printf("ASIO device: %d\n", asioDevice);

	PaStreamParameters streamParams;
	streamParams.channelCount = 2;
	streamParams.device = asioDevice;
	streamParams.hostApiSpecificStreamInfo = NULL;
	streamParams.suggestedLatency = 0.0;
	streamParams.sampleFormat = paFloat32;

	printf("Opening stream...\n");
	PaStream* stream;
	perr = Pa_OpenStream(&stream, 
		NULL, 
		&streamParams, 
		AUDIO_RATE, 
		paFramesPerBufferUnspecified,
		paClipOff | paDitherOff,
		audioCallback,
		nullptr);

	if (perr != paNoError) {
		puts(Pa_GetErrorText(perr));
		assert("couldn't open stream" && false);
	}

	if ((perr = Pa_StartStream(stream)) != paNoError) {
		puts(Pa_GetErrorText(perr));
		assert("couldn't start stream" && false);
	}

	HANDLE thePipe;

	logger->info("Setting up pipes");
	size_t outputSize = 8 * 1024 * 1024;
	size_t inputSize = 1 * 1024 * 1024;
	// FILE_FLAG_OVERLAPPED might be useful
	thePipe = CreateNamedPipe("\\\\.\\pipe\\lines", PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE, 1, outputSize, inputSize, 0, NULL);
	int err = GetLastError();
	assert(thePipe != INVALID_HANDLE_VALUE);

	logger->info("waiting for client..");
	ConnectNamedPipe(thePipe, NULL);

	std::vector<SCOPE_Line> linelist;
	std::mutex linelistBarrier; // protects linelist, linelist_visible, lineCount, producerLineIndex

	bool visuals = true;
	int screenReady = 0;
	int lineCount = 0;
	int producerLineIndex = 0; // updated inside thread
	unsigned int frameCount = 0;
	bool killProducerThread = false;
	int lineBurstSize = 800;
	int bufferFillThreshold = 2250;

	std::chrono::milliseconds sleepTime(1);
	std::thread producer([
		&linelistBarrier, 
		&lineCount, 
		&linelist, 
		&sleepTime, 
		&producerLineIndex, 
		&killProducerThread,
		&lineBurstSize,
		&bufferFillThreshold](){
		while (!killProducerThread) {
			if (linesynth.lineQueue.size() < bufferFillThreshold)
				{
					int limit = min(lineCount, producerLineIndex + lineBurstSize);
					std::lock_guard<std::mutex> linelistLock(linelistBarrier);

					for (int i = producerLineIndex; i < limit; i++) {
						if (linelist[i].visible) {
							std::lock_guard<std::mutex> lock(linesynth.lineQueueMutex);
							linesynth.lineQueue.push(linelist[i]);
						}
					}

					producerLineIndex = producerLineIndex + lineBurstSize;
					// wrap around
					if (producerLineIndex >= lineCount) {
						producerLineIndex = 0;
					}
				}

			std::this_thread::sleep_for(sleepTime);
		}
	});

	logger->info("Starting main loop");
	while (!input.keyHit(SDL_SCANCODE_ESCAPE)) 
	{
		if (visuals)
			glClear(GL_COLOR_BUFFER_BIT);
		input.processEvents();

		if (input.keyHit(SDL_SCANCODE_UP)) {
			linesynth.lineDrawSpeed += 5.0f;
			printf("drawspeed: %f\n", linesynth.lineDrawSpeed);
		} else if (input.keyHit(SDL_SCANCODE_DOWN)) {
			linesynth.lineDrawSpeed -= 5.0f;
			printf("drawspeed: %f\n", linesynth.lineDrawSpeed);
		}

		if (input.keyHit(SDL_SCANCODE_S)) {
			lineBurstSize += 20;
			printf("lineBurstSize: %d\n", lineBurstSize);
		} else if (input.keyHit(SDL_SCANCODE_A)) {
			lineBurstSize -= 20;
			printf("lineBurstSize: %d\n", lineBurstSize);
		}

		if (input.keyHit(SDL_SCANCODE_X)) {
			bufferFillThreshold += 20;
			printf("bufferFillThreshold: %d\n", bufferFillThreshold);
		} else if (input.keyHit(SDL_SCANCODE_Z)) {
			bufferFillThreshold -= 20;
			printf("bufferFillThreshold: %d\n", bufferFillThreshold);
		}

		if (input.keyHit(SDL_SCANCODE_R)) {
			if (recordingAudio) {
				recordingAudio = false;

				std::string audioFilePath("C:\\Users\\Public\\recording");
				audioFilePath += std::to_string(rand());
				audioFilePath += ".raw";
				printf("finished recording. saving %d floats to %s\n", recordedAudioBuffer.size(), audioFilePath.c_str());
				FILE* fp = fopen(audioFilePath.c_str(), "wb");
				printf("fp: %d\n", fp);
				size_t bytesWritten = fwrite(&recordedAudioBuffer[0], sizeof(float), recordedAudioBuffer.size(), fp);
				printf("bytesWritten: %u\n", bytesWritten);
				fclose(fp);
			} else {
				recordedAudioBuffer.clear();
				recordedAudioBuffer.reserve(3 * 60 * 48000 * 2);
				recordingAudio = true;
				printf("Recording audio...\n");
			}
			printf("bufferFillThreshold: %d\n", bufferFillThreshold);
		}



		visuals ^= input.keyHit(SDL_SCANCODE_V);

		{
			int inputLineCount = -1;
			int bytesRead = -1;
			int bytesAvailable = -1;

			if (PeekNamedPipe(
				thePipe,
				&inputLineCount,
				sizeof(inputLineCount),
				(LPDWORD)&bytesRead,
				(LPDWORD)&bytesAvailable,
				NULL)
				&& (bytesAvailable >= 4)) {
				if (bytesAvailable == (sizeof(SCOPE_Line) * inputLineCount + sizeof(int))) {
					std::lock_guard<std::mutex> linelistLock(linelistBarrier);
					int dummy;
					// read off the size we actually got in PeekNamedPipe already
					int read = ReadFile(thePipe, &dummy, sizeof(int), (LPDWORD)&bytesRead, NULL);

					lineCount = inputLineCount;

					if (linelist.size() <= inputLineCount) {
						linelist.resize(inputLineCount + 1);
					}

#if DEBUG_PRINTS
					printf("Data available: ");
#endif

					read = ReadFile(
						thePipe,
						&linelist[0],
						inputLineCount*sizeof(SCOPE_Line),
						(LPDWORD)&bytesRead,
						NULL);
#if DEBUG_PRINTS
					printf("read %d/%d bytes. new lines available: %d\n", bytesRead, bytesAvailable - sizeof(int), inputLineCount);
#endif

					SCOPE_Line newline;
					for (int i = 0; i < lineCount; i++) {
						auto& line = linelist[i];
						line.points[0].x -= 0.5f;
						line.points[0].y = -(line.points[0].y - 0.5f);
						line.points[1].x -= 0.5f;
						line.points[1].y = -(line.points[1].y - 0.5f);

						//linelist[i] = line;
					}

					//memset(&linelist_visible[0], 0, sizeof(bool)*linelist_visible.size()); // only for printing
					//std::fill(linelist_visible.begin(), linelist_visible.end(), false);
					if (lineCount > lineBurstSize) {
						producerLineIndex = rand() % (lineCount - lineBurstSize); // producer starts from a random offset
					} else {
						producerLineIndex = 0;
					}
					//linesynth.lineQueue.push(linelist.begin(), linelist.begin() + lineCount);
					screenReady = 0;
				}
			}
		}

		if (visuals) {
			glBegin(GL_LINES);
			for (int i = 0; i < lineCount; i++) {
				const auto& l = linelist[i];
				if (!l.visible)
					continue;

				glVertex3f(l.points[0].x, l.points[0].y, 0.0f);
				glVertex3f(l.points[1].x, l.points[1].y, 0.0f);
			}
			glEnd();
		}

#if DEBUG_PRINTS
		if (frameCount % 10 == 0) {
			printf("producer: %d/%d,\tconsumer: %d,\tready: %d\t%d/%d\n", producerLineIndex, lineCount, linesynth.lineQueue.size(),
				screenReady,
				lineBurstSize,
				bufferFillThreshold);
		}
#endif

		//if (!screenReady && (lineCount - producerLineIndex) < 500) {
		if (!screenReady) {
			int written = -1;
			screenReady = 1;
			if (WriteFile(thePipe, &screenReady, sizeof(screenReady), (LPDWORD)&written, NULL)) {
#ifdef DEBUG_PRINTS
				printf("Requesting lines. Status: %d\n", written);
#endif
			} else {
				screenReady = 0; // try again on next frame
			}
		}

		static int lastOverflowCount;
		if (bufferUnderflowCount != lastOverflowCount) {
			printf("UNDERFLOWS: %d\n", bufferUnderflowCount);
			lastOverflowCount = bufferUnderflowCount;
		}

		frameCount++;

		if (visuals) {
			win.draw();
		}
	}

	programEnded = true;
	killProducerThread = true;
	producer.join();

	if ((perr = Pa_StopStream(stream)) != paNoError) {
		puts(Pa_GetErrorText(perr));
	};

	if ((perr = Pa_CloseStream(stream)) != paNoError) {
		puts(Pa_GetErrorText(perr));
	};

	perr = Pa_Terminate();
	if (perr != paNoError) {
		puts(Pa_GetErrorText(perr));
	}

	//audio_asio_kill();
	logger->info("Program finished");


	return 0;
}