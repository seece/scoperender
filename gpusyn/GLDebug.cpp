#include <conio.h>
#include "GLDebug.h"
#include "Logger.h"
using eng::logger;

namespace {
	GLchar* getErrorString(GLenum errorCode) 
	{
		/* GL Errors */
		if (errorCode == GL_NO_ERROR) {
			return (GLchar*) "no error";
		}
		else if (errorCode == GL_INVALID_VALUE) {
			return (GLchar *) "invalid value";
		}
		else if (errorCode == GL_INVALID_ENUM) {
			return (GLchar *) "invalid enum";
		}
		else if (errorCode == GL_INVALID_OPERATION) {
			return (GLchar *) "invalid operation";
		}
		else if (errorCode == GL_STACK_OVERFLOW) {
			return (GLchar *) "stack overflow";
		}
		else if (errorCode == GL_STACK_UNDERFLOW) {
			return (GLchar *) "stack underflow";
		}
		else if (errorCode == GL_OUT_OF_MEMORY) {
			return (GLchar *) "out of memory";
		}

		return (GLchar*) "unknown";
	}
}

bool eng::gl::checkContextExists()
{
	HGLRC context = wglGetCurrentContext();
	return context != NULL;
}

// modified from http://openglbook.com/the-book/chapter-4-entering-the-third-dimension/
void eng::gl::assertGlError(const char * error_message, bool halt /* = true */)
{
	#ifdef _DEBUG
	const GLenum ErrorValue = glGetError();

	if (ErrorValue == GL_NO_ERROR)
		return;

	const char* APPEND_DETAIL_STRING = ": %s\n";
	const size_t APPEND_LENGTH = strlen(APPEND_DETAIL_STRING) + 1;
	const size_t message_length = strlen(error_message);
	char* display_message = (char*)malloc(message_length + APPEND_LENGTH);

	memcpy(display_message, error_message, message_length);
	memcpy(&display_message[message_length], APPEND_DETAIL_STRING, APPEND_LENGTH);

	logger->error(display_message, getErrorString(ErrorValue));

	free(display_message);

	if (halt) {
		_getch();
		exit(EXIT_FAILURE);
	}
	#endif
}

void formatDebugOutputAMD(char outStr[], size_t outStrSize, GLenum category, GLuint id,
                         GLenum severity, const char *msg)
{
    char categoryStr[32];
    const char *categoryFmt = "UNDEFINED(0x%04X)";
    switch(category)
    {
    case GL_DEBUG_CATEGORY_API_ERROR_AMD:          categoryFmt = "API_ERROR"; break;
    case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:      categoryFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_CATEGORY_DEPRECATION_AMD:        categoryFmt = "DEPRECATION"; break;
    case GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD: categoryFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_CATEGORY_PERFORMANCE_AMD:        categoryFmt = "PERFORMANCE"; break;
    case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:    categoryFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_CATEGORY_APPLICATION_AMD:        categoryFmt = "APPLICATION"; break;
    case GL_DEBUG_CATEGORY_OTHER_AMD:              categoryFmt = "OTHER"; break;
    }
    _snprintf(categoryStr, 32, categoryFmt, category);

    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_AMD:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_AMD: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_AMD:    severityFmt = "LOW";    break;
    }
    _snprintf(severityStr, 32, severityFmt, severity);

    _snprintf(outStr, outStrSize, "OpenGL: %s [category=%s severity=%s id=%d]",
        msg, categoryStr, severityStr, id);
}

// see http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
void CALLBACK debugCallbackAMD(GLuint id, GLenum category, GLenum severity, GLsizei length,
                     const GLchar *message, GLvoid *userParam)
{
    (void)length;
    FILE *outFile = (FILE*)userParam;
    char finalMsg[256];
    formatDebugOutputAMD(finalMsg, 256, category, id, severity, message);
	logger->debug(finalMsg);
}

void formatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type,
    GLuint id, GLenum severity, const char *msg)
{
    char sourceStr[32];
    const char *sourceFmt = "UNDEFINED(0x%04X)";
    switch(source)

    {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
    }

    _snprintf(sourceStr, 32, sourceFmt, source);
 
    char typeStr[32];
    const char *typeFmt = "UNDEFINED(0x%04X)";
    switch(type)
    {

    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
    }
    _snprintf(typeStr, 32, typeFmt, type);

 
    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
    }

    _snprintf(severityStr, 32, severityFmt, severity);
 
    _snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
        msg, sourceStr, typeStr, severityStr, id);
}

void CALLBACK debugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
                     GLsizei length, const GLchar *message, GLvoid *userParam)
{
    (void)length;
    FILE *outFile = (FILE*)userParam;
    char finalMessage[256];
    formatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);
	logger->debug(finalMessage);
}

#define DEBUG_CATEGORY_API_ERROR_AMD                    0x9149
#define DEBUG_CATEGORY_WINDOW_SYSTEM_AMD                0x914A
#define DEBUG_CATEGORY_DEPRECATION_AMD                  0x914B
#define DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD           0x914C
#define DEBUG_CATEGORY_PERFORMANCE_AMD                  0x914D
#define DEBUG_CATEGORY_SHADER_COMPILER_AMD              0x914E
#define DEBUG_CATEGORY_APPLICATION_AMD                  0x914F
#define DEBUG_CATEGORY_OTHER_AMD                        0x9150

#define DEBUG_SEVERITY_HIGH_AMD                         0x9146
#define DEBUG_SEVERITY_MEDIUM_AMD                       0x9147
#define DEBUG_SEVERITY_LOW_AMD                          0x9148

void eng::gl::setupDebugOutput()
{
	if (glext_AMD_debug_output) {
		logger->info("AMD_debug_output found");
		glDebugMessageCallbackAMD(debugCallbackAMD, stderr);
		// we want to handle failed shader compilation ourselves
		glDebugMessageEnableAMD(DEBUG_CATEGORY_SHADER_COMPILER_AMD, DEBUG_SEVERITY_HIGH_AMD, 0, NULL, false);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	} else if (glext_ARB_debug_output) {
		logger->info("ARB_debug_output found");

		glDebugMessageCallback((GLDEBUGPROC)debugCallbackARB, stderr);
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	} else {
		logger->info("No debug output detected.");
	}
}