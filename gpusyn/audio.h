/*
* The audio back end. 
*/

#pragma once

#include <climits>
#include <cstdint>
#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>
#include <mmreg.h>

#include "config.h"
#include "event.h"
#include "SynthInterface.h"

#define AUDIO_MAX_EVENTS (16384)

static HWAVEOUT	hWaveOut;

static WAVEFORMATEX WaveFMT =
{
	#ifdef FLOAT_32BIT	
		WAVE_FORMAT_IEEE_FLOAT,
	#else
		WAVE_FORMAT_PCM,
	#endif		
	2, // channels
	AUDIO_RATE, // samples per sec
	AUDIO_RATE*sizeof(SAMPLE_TYPE)*2, // bytes per sec
	sizeof(SAMPLE_TYPE)*2, // block alignment;
	sizeof(SAMPLE_TYPE)*8, // bits per sample
	0 // extension not needed
};

static WAVEHDR WaveHDR = 
{
	NULL,	// this will be filled in later 
	AUDIO_BUFFERSIZE*sizeof(SAMPLE_TYPE)*2,
	0, 
	0, 
	0, 
	0, 
	0, 
	0
};

struct Buffer {
	WAVEHDR header;
	SAMPLE_TYPE data[AUDIO_BUFFERSIZE*2];
};

namespace synth {
//typedef void (*SynthRender_t)(SAMPLE_TYPE *, int, eventbuffer_t*);
//typedef void (*PollEventCallback_t)(eventbuffer_t* buffer, int samplecount);

void audio_init();
void audio_poll(SynthInterface& synth_to_use);
void audio_free(void);
}
