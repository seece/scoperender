#pragma once

#define M_NOTEMAGIC 1.059460646483
#define M_TAU (2.0*PI)
#define M_NOTEFREQ(pitch) (pow(M_NOTEMAGIC, pitch) * 440.0)

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#define saturate(a) min(1.0, max(0.0, a))