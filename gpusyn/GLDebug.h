/*
 * OpenGL debugging utilities.
 */
#pragma once

#include "opengl.h"

namespace eng {
namespace gl {
void assertGlError(const char* error_message, bool halt=true);
bool checkContextExists();
void setupDebugOutput();
}
}