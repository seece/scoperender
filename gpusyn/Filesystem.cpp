#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <string>
#include <Windows.h>
#include "filesystem.h"
#include "logger.h"

using eng::logger;

namespace eng {
namespace fsys {
std::string extractBasename(const std::string& path)
{
	char drive[_MAX_DRIVE]; 
	char dir[_MAX_DIR];
	char basename[_MAX_FNAME];
	char extension[_MAX_EXT];
	_splitpath(path.c_str(), drive, dir, basename, extension);

	return std::string(basename);
}

File::File(const std::string& path) :
	filepath(path),
	stringdata(nullptr),
	size(0),
	lastModified(0),
	fullpath(filepath)
{
	load(fullpath.c_str());
}

void File::load(const char * path) 
{
	size_t result;
	data = nullptr;
	size = 0;

	FILE * fp;
	fp = fopen(path, "rb");

	if (fp == nullptr) {
		logger->error("Couldn't open file %s", path);
		return;
	}

	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	data = new char[size];
	if (data == nullptr) {
		logger->error("Memory error when loading file %s", path);
		delete[] data;
		data = nullptr;
		fclose(fp);
		return;
	}

	result = fread(data,1,size,fp);

	if (result != size) {
		logger->info("Warning: only read %u / %u bytes from %s", result, size, path);
	}

	fclose(fp);

	updateLastModified();
}

File::~File() 
{
	if (stringdata != nullptr) {
		delete[] stringdata;
	}

	if (data == nullptr || size == 0) {
		return;
	}

	delete this->data;
}

const void File::print() 
{
	if (this->data == nullptr || this->size == 0) {
		return;
	}

	for (uint32_t i=0;i<size;i++) {
		std::cout << data[i];
	}
}

char* File::c_str()
{
	if (stringdata == nullptr) {
		stringdata = new char[this->size + 1];
		memcpy(stringdata, data, size);
		stringdata[size] = '\0';
	}

	return stringdata;
} 

const std::string File::getBasename()
{
	char drive[_MAX_DRIVE]; 
	char dir[_MAX_DIR];
	char basename[_MAX_FNAME];
	char extension[_MAX_EXT];
	_splitpath(filepath.c_str(), drive, dir, basename, extension);

	return std::string(basename);
}

uint64_t File::reload()
{
	if (data != nullptr) {
		delete[] data;	
		data = nullptr;
	}

	if (stringdata != nullptr) {
		delete[] stringdata;
		stringdata = nullptr;
	}

	load(fullpath.c_str());

	return size;
}

void File::updateLastModified()
{
	HANDLE handle  = CreateFile(fullpath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (handle == INVALID_HANDLE_VALUE) {
		logger->error("Couldn't open file %s for read in updateLastModified", filepath.c_str());			
		return;
	}

	FILETIME modTime;
	GetFileTime(handle, NULL, NULL, &modTime);
	CloseHandle(handle);

	// the 64 bit integer is returned in two parts, so we need to join them
	uint64_t last = (static_cast<uint64_t>(modTime.dwHighDateTime) << 32) | 
		static_cast<uint64_t>(modTime.dwLowDateTime);

	lastModified = last;
}

unsigned long long File::getLastModified()
{
	updateLastModified();
	return lastModified;
}

bool File::beenModified()
{
	unsigned long long last = lastModified;	
	updateLastModified();

	if (lastModified > last) {
		return true;
	}

	return false;
}

std::string File::getFilePath()
{
	return filepath;
}

std::string File::getFullPath()
{
	return fullpath;
}
} // namespace eng
} // namespace fsys
