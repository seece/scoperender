#pragma once
#include "SynthInterface.h"
#include <cstdint>
#include <queue>
#include <thread>
#include <mutex>

typedef struct SCOPE_Point {
	float x;
	float y;
	float z;
	float screen_x;
	float screen_y;
	SCOPE_Point() {};
	SCOPE_Point(float x, float y) : x(x), y(y), z(0.0f) {};
	SCOPE_Point(float x, float y, float z) : x(x), y(y), z(z) {};
} SCOPE_Point_t;

typedef struct SCOPE_Line {
	SCOPE_Point_t points[2];
	bool visible;
	SCOPE_Line() {};
	SCOPE_Line(float x1, float y1, float x2, float y2) {
		points[0].x = x1;
		points[0].y = y1;
		points[1].x = x2;
		points[1].y = y2;
	};
	SCOPE_Line(const SCOPE_Point_t& p1, const SCOPE_Point_t& p2) {
		points[0] = p1;
		points[1] = p2;
	}
} SCOPE_Line_t;


class LineSynth : public SynthInterface {
	public:

	float lineDrawSpeed;
	LineSynth(float rate);
	virtual ~LineSynth();

	//virtual void update(synth::eventbuffer_t& events, int samplecount);
	virtual void render(SAMPLE_TYPE* buffer, int samplecount);
	std::queue<SCOPE_Line> lineQueue;
	std::mutex lineQueueMutex;

	protected:
	uint64_t frames_rendered; // how many stereo frames we have rendered (the timer)
	float rate;

	int queuePos; // index in lineQueue
	bool working;
	SCOPE_Line target;
	int currentStep;
	int lineStepCount;
	//double lineProgress; // progress through the current line

	private:
	LineSynth(const LineSynth&); // disallow copy constructor
	LineSynth& operator=(const LineSynth&); // disallow assignment
};