#pragma once

#define AUDIO_NUMCHANNELS (2)
#define AUDIO_RATE        (48000.0f)
#define AUDIO_BUFFERSIZE  (4096)
#define AUDIO_BUFFERS (3)

#define FLOAT_32BIT
#ifdef FLOAT_32BIT
	#define SAMPLE_TYPE float
#else
	#define SAMPLE_TYPE short
#endif

