#include "audio_asio.h"

#include <asiosys.h>
#include <asio.h>
#include <asiodrivers.h>

#include <cstdio>
#include <cassert>

#include <ASIOConvertSamples.h>

#define ASIOTRY(expr) do { ASIOError err = expr; if (err != ASE_OK) {printf("%s failed with error %d = '%s'.", #expr, err, PaAsio_GetAsioErrorText(err)); die("ASIO fail: " #expr);} } while (0);

// This is used by asio.cpp via extern
AsioDrivers* asioDrivers = 0;

namespace {
struct LocalState {
	ASIOCallbacks callbacks;
	char _pad1[16];
	char _pad2[16];
	audio_asio_callback_t userCallback;
	char _pad3[16];
	int audioBufferSize;
	char _pad4[16];
	bool audioShuttingDown;
	char _pad5[16];
	double samplerate;
	void* hwnd;
} state;

ASIOBufferInfo bufferInfos[2] = { { false, 0, { nullptr, nullptr } }, { false, 1, { nullptr, nullptr } } };
//float* userBuffers[2] = { nullptr, nullptr };

static const char* PaAsio_GetAsioErrorText(ASIOError asioError)
{
	const char *result;

	switch (asioError){
	case ASE_OK:
	case ASE_SUCCESS:           result = "Success"; break;
	case ASE_NotPresent:        result = "Hardware input or output is not present or available"; break;
	case ASE_HWMalfunction:     result = "Hardware is malfunctioning"; break;
	case ASE_InvalidParameter:  result = "Input parameter invalid"; break;
	case ASE_InvalidMode:       result = "Hardware is in a bad mode or used in a bad mode"; break;
	case ASE_SPNotAdvancing:    result = "Hardware is not running when sample position is inquired"; break;
	case ASE_NoClock:           result = "Sample clock or rate cannot be determined or is not present"; break;
	case ASE_NoMemory:          result = "Not enough memory for completing the request"; break;
	default:                    result = "Unknown ASIO error"; break;
	}

	return result;
}

const char* AsioSampleTypeString(ASIOSampleType type)
{
	switch (type) {
	case ASIOSTInt16MSB:  return "ASIOSTInt16MSB";  break;
	case ASIOSTInt16LSB:  return "ASIOSTInt16LSB";  break;
	case ASIOSTFloat32MSB:return "ASIOSTFloat32MSB"; break;
	case ASIOSTFloat32LSB:return "ASIOSTFloat32LSB"; break;
	case ASIOSTFloat64MSB:return "ASIOSTFloat64MSB"; break;
	case ASIOSTFloat64LSB:return "ASIOSTFloat64LSB"; break;
	case ASIOSTInt32MSB:  return "ASIOSTInt32MSB";  break;
	case ASIOSTInt32LSB:  return "ASIOSTInt32LSB";  break;
	case ASIOSTInt32MSB16:return "ASIOSTInt32MSB16"; break;
	case ASIOSTInt32LSB16:return "ASIOSTInt32LSB16"; break;
	case ASIOSTInt32MSB18:return "ASIOSTInt32MSB18"; break;
	case ASIOSTInt32MSB20:return "ASIOSTInt32MSB20"; break;
	case ASIOSTInt32MSB24:return "ASIOSTInt32MSB24"; break;
	case ASIOSTInt32LSB18:return "ASIOSTInt32LSB18"; break;
	case ASIOSTInt32LSB20:return "ASIOSTInt32LSB20"; break;
	case ASIOSTInt32LSB24:return "ASIOSTInt32LSB24"; break;
	case ASIOSTInt24MSB:  return "ASIOSTInt24MSB";  break;
	case ASIOSTInt24LSB:  return "ASIOSTInt24LSB";  break;
	default:              return "Unknown "; break;

	}
}

}

#define ANNOYING_DEATH

static void die(const char* msg)
{
#ifdef ANNOYING_DEATH
	MessageBox(0, msg, "fak u", MB_OK | MB_ICONSTOP);
#else
	puts(msg);
#endif
	exit(1);
}

void bufferSwitchCallback(long doubleBufferIndex, ASIOBool directProcess)
{
	if (state.audioShuttingDown) {
		return;
	}
	return;

	//printf("bufferSwitchCallback %d %d\n", doubleBufferIndex, directProcess);
	void* left = &bufferInfos[0].buffers[doubleBufferIndex];
	void* right = &bufferInfos[1].buffers[doubleBufferIndex];

	// FIXME audioBufferSize is broken, the value is just hardcoded
	int samples = 256;

	/*
	userCallback(
		(float*)left, 
		(float*)right,
		samples);
		*/

	ASIOConvertSamples::float32toInt32inPlace((float*)left, samples);
	ASIOConvertSamples::float32toInt32inPlace((float*)right, samples);

	for (int i = 0; i < samples; i++) {
		((int*)left)[i] = INT_MAX * rand();
		((int*)right)[i] = (i % 1000) * (INT_MAX/4);
	}
}

void sampleRateDidChangeCallback(ASIOSampleRate sRate)
{
	puts(__FUNCTION__);
}

long asioMessageCallback(long selector, long value, void* message, double* opt)
{
	printf("msg: %d\n", selector);
	// currently the parameters "value", "message" and "opt" are not used.
	long ret = 0;
	switch (selector)
	{
	case kAsioSelectorSupported:
		if (value == kAsioResetRequest
			|| value == kAsioEngineVersion
			|| value == kAsioResyncRequest
			|| value == kAsioLatenciesChanged
			|| value == kAsioSupportsTimeInfo
			|| value == kAsioSupportsTimeCode
			|| value == kAsioSupportsInputMonitor
			|| value == kAsioOverload)
			ret = 1L;
		break;
	case kAsioResetRequest:
		audio_asio_kill();
		audio_asio_init(state.hwnd, state.samplerate);
		return 1;
	case kAsioResyncRequest:
		return 1;
	case kAsioLatenciesChanged:
		return 1;
	case kAsioEngineVersion:
		return 2;
	case kAsioSupportsTimeInfo:
		return 0;
	case kAsioSupportsTimeCode:
		return 0;
	case kAsioOverload:
		printf("Overload!\n");
		return 0;
	}
	return 0;
}

ASIOTime* bufferSwitchTimeInfoCallback(ASIOTime* params, long doubleBufferIndex, ASIOBool directProcess)
{
	puts(__FUNCTION__);

	return params;
}

void audio_asio_init(void* hwnd, double samplerate)
{
	memset(&state, 0, sizeof(state));
	if (!asioDrivers) {
		asioDrivers = new AsioDrivers();
	}
	state.audioShuttingDown = false;
	state.hwnd = hwnd;
	state.samplerate = samplerate;

	{
		ASIOError err = ASE_OK;
		int deviceCount = asioDrivers->asioGetNumDev();
		printf("asio devices: %d\n", deviceCount);

		if (deviceCount == 0) {
			die("No ASIO devices found!");
		}

		for (int i = 0; i < deviceCount; i++) {
			char name[100];
			asioDrivers->asioGetDriverName(i, name, 100);
			puts(name);
		}

		int deviceIndex = 1;
		
		char driverName[100] = { 0 };
		asioDrivers->asioGetDriverName(deviceIndex, driverName, sizeof(driverName)/sizeof(char));

		void* someBuffer;
		//int openResult = drivers.asioOpenDriver(deviceIndex, &someBuffer);
		printf("Trying to load %s...\n", driverName);
		bool loadResult = asioDrivers->loadDriver(driverName); // result is the driver index?
		printf("driver load: %s\n", loadResult ? "success" : "fail");



		ASIODriverInfo info;
		info.asioVersion = 2;
		info.sysRef = hwnd;
		info.driverVersion = 0;			// written by driver
		info.name[0] = '\0';			// written by driver
		info.errorMessage[0] = '\0';	// written by driver
		err = ASIOInit(&info);

		if (err == ASE_OK) {
			puts("Init OK!");
			printf("Initialized '%s', version %d.\n", info.name, info.driverVersion);
		} else {
			printf("Init error %d: '%s'\n", err , info.errorMessage);
			die("Couldn't init ASIO.");
		}

		bool postOutput = ASIOOutputReady() == ASE_OK;
		printf("postOutput: %d\n", postOutput);

		long inputChannelsCount = -1;
		long outputChannelsCount = -1;
		err = ASIOGetChannels(&inputChannelsCount, &outputChannelsCount);
		if (err != ASE_OK) die("Couldn't fetch channel info.");
		printf("%d input and %d output channels.\n", inputChannelsCount, outputChannelsCount);
		long minSize, maxSize, preferredSize, granularity;
		ASIOGetBufferSize(&minSize, &maxSize, &preferredSize, &granularity);
		printf("Buffer sizes %d - %d, preferred %d, granularity %d\n", minSize, maxSize, preferredSize, granularity);

		ASIOTRY(ASIOCanSampleRate(samplerate)); // just abort if we can't set it
		ASIOTRY(ASIOSetSampleRate(samplerate)); 

		puts("Output channels:");
		for (int i = 0; i < outputChannelsCount; i++) {
			ASIOChannelInfo cInfo;
			cInfo.channel = i;
			cInfo.isInput = false;
			cInfo.name[0] = '\0';	// written by driver
			cInfo.isActive = false; // written by driver
			cInfo.type = 0;			// written by driver
			ASIOError err = ASIOGetChannelInfo(&cInfo);
			if (err != ASE_OK) {
				printf("Couldn't fetch channel %d info.", i);
				die("channel init error");
			}

			// type 18 - 32 bit int
			printf(" - %d %u: %s %s\n", i, cInfo.type, cInfo.name, cInfo.isActive ? "ACTIVE" : "INACTIVE");
		}

		state.callbacks.asioMessage = asioMessageCallback;
		state.callbacks.bufferSwitch = bufferSwitchCallback;
		state.callbacks.bufferSwitchTimeInfo = bufferSwitchTimeInfoCallback;
		state.callbacks.sampleRateDidChange = sampleRateDidChangeCallback;

		err = ASIOCreateBuffers(bufferInfos, outputChannelsCount, preferredSize, &state.callbacks);
		state.audioBufferSize = preferredSize;
		//assert(audioBufferSize == 512); // FIXME a hack, 512 buffer size is hardcoded in the callback

		if (err != ASE_OK) {
			printf("Create buffers failed with error %d!\n", err);
			die("Create buffers failed");
		}

		/*
		userBuffers[0] = new float[audioBufferSize];
		userBuffers[1] = new float[audioBufferSize];
		*/

		//ASIOControlPanel();
	}
}

void audio_asio_start()
{
	assert(state.userCallback != nullptr); // user callback needs to be set before starting audio playback
	ASIOTRY(ASIOStart());
}

void audio_asio_kill()
{
	state.audioShuttingDown = true; // signal for audio thread
	ASIOTRY(ASIOStop());
	ASIOTRY(ASIODisposeBuffers());
	ASIOTRY(ASIOExit());
	assert(asioDrivers);
	delete asioDrivers;
	asioDrivers = 0;
	state.userCallback = 0;
	/*
	delete[] userBuffers[0];
	delete[] userBuffers[1];
	*/
}

void audio_asio_set_callback(audio_asio_callback_t callback)
{
	state.userCallback = callback;
}
