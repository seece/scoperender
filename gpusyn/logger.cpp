#define WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN
#include <iostream>
#include <cstdarg>
#include <conio.h>
#include <Windows.h>
#include "Logger.h"
#include <io.h>
#include <fcntl.h>

std::unique_ptr<LogSink> eng::logger;

LogSink::LogSink(const char* filename) 
{
#ifdef _DEBUG
	openConsoleWindow();
#endif
}

LogSink::~LogSink() {};

void LogSink::info(const char* format, ...)
{
	va_list argptr;
	va_start(argptr, format);
	write(format, argptr, stderr);
	fprintf(stdout, "\n");
}

void LogSink::debug(const char* format, ...)
{
#ifdef _DEBUG
	fprintf(stderr, "DEBUG: ");
	va_list argptr;
	va_start(argptr, format);
	write(format, argptr, stderr);
	fprintf(stdout, "\n");
#endif
}

void LogSink::error(const char* format, ...)
{
	fprintf(stderr, "ERROR: ");

	va_list argptr;
	va_start(argptr, format);

	// Show a message box in release mode
#ifdef _RELEASE
	write(format, argptr, stderr, true);
	exit(1);
#else
	write(format, argptr, stderr);
#endif
	fprintf(stderr, "\n");

	
#ifdef _DEBUG
	// Halt on error when running in Debug-mode.
	_getch();
#endif

}

void LogSink::write(const char* format, va_list argptr, FILE* output, bool messagebox)
{
	char message[4096] = {0};

	vsprintf_s(message, sizeof(message), format, argptr);
	va_end(argptr);

	fprintf(output, message);

	if (messagebox)
		MessageBox(GetActiveWindow(), message, "An error occured", MB_OK | MB_ICONWARNING);	
}

void LogSink::openConsoleWindow()
{
	// from http://justcheckingonall.wordpress.com/2008/08/29/console-window-win32-app/
	// Redirects stdout and stderr to the opened console.
	AllocConsole();

    HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;

    HANDLE handle_err = GetStdHandle(STD_ERROR_HANDLE);
	int hErrorCrt = _open_osfhandle((long) handle_err, _O_TEXT);
	FILE* hf_err = _fdopen(hErrorCrt, "w");
	setvbuf(hf_err, NULL, _IONBF, 1);
	*stderr = *hf_err;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;
}