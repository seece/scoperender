#pragma once
#include "config.h"
#include "event.h"

class SynthInterface {
	public:
	//virtual void update(synth::eventbuffer_t& events, int samplecount) = 0;
	virtual void render(SAMPLE_TYPE* buffer, int samplecount) = 0;
};