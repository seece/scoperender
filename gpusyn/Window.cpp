#include <stdint.h>
#include <SDL.h>
#include <SDL_syswm.h>
#include "opengl.h"
#include "GLDebug.h"
#include "Window.h"
#include "Logger.h"

using eng::logger;

namespace {
void checkSDLError(int line = -1)
{
	const char *error = SDL_GetError();
	if (*error != '\0')
	{
		if (line != -1) {
			logger->info("Line: %i", line);
		}

		logger->error("SDL Error: %s", error);
		SDL_ClearError();
	}
}
}

Window::Window(const Window::Settings& settings) : 
	mainWindow(nullptr), 
	mainContext(nullptr), 
	currentSettings(settings),
	startTime(GetTickCount())
{
	initSDL(settings);
}

Window::~Window() 
{
	if (mainContext) {
		SDL_GL_DeleteContext(mainContext);
	}

	if (mainWindow) {
		SDL_DestroyWindow(mainWindow);
	}

	SDL_Quit();
}


Window::Settings Window::getSettings() const
{
	return currentSettings;
}

void Window::draw()
{
	SDL_GL_SwapWindow(mainWindow);
}

void Window::initSDL(const Window::Settings& settings) 
{
	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY); 

	#ifdef _DEBUG
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	#endif

	uint32_t flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

	if (settings.fullscreen) {
		flags |= SDL_WINDOW_FULLSCREEN;
	}

	mainWindow = SDL_CreateWindow("DemoApp", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		settings.width, settings.height, flags);

	if (!mainWindow) {
		logger->error("Couldn't create SDL window.");
	}

	checkSDLError(__LINE__);

	mainContext = SDL_GL_CreateContext(mainWindow);

	if (!mainWindow) {
		logger->error("Couldn't create OpenGL context.");
	}

	checkSDLError(__LINE__);

	if(ogl_LoadFunctions() == ogl_LOAD_FAILED) {
		logger->error("Couldn't init OpenGL functions!");
	}

	SDL_GL_SetSwapInterval(settings.vsync ? 1 : 0);
	logger->info("VSYNC: %s", settings.vsync ? "on" : "off");

	logger->info("SDL initialized.");

	logger->info("GL_VERSION: %s", glGetString(GL_VERSION));

	#ifdef _DEBUG
	#ifndef _DEBUG_NO_GL
	eng::gl::setupDebugOutput();
	#endif
	#endif


	SDL_SysWMinfo info;
	SDL_GetWindowWMInfo(mainWindow, &info);
	this->windowHandle = info.info.win.window;
}

void Window::cleanupSDL()
{
	SDL_GL_DeleteContext(mainContext);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
}

double Window::getTime()
{
	return (GetTickCount() - startTime) / 1000.0;
}

void* Window::getWindowHandle()
{
	return windowHandle;
}
