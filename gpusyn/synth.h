#pragma once
#include <memory>
#include <cstdint>
#include "SynthInterface.h"
#include "FBO.h"
#include "Filesystem.h"
#include "ShaderProgram.h"
#include "event.h"

class Synth : public SynthInterface {
	public:

	static const int EVENT_LIST_ENTRIES = 64;
	static const int EVENT_LIST_SIZE = sizeof(synth::Event) * EVENT_LIST_ENTRIES;
	static const int UNIFORM_BLOCK_EVENT_LIST = 0;

	Synth();
	virtual ~Synth();

	//virtual void update(synth::eventbuffer_t& events, int samplecount);
	virtual void render(SAMPLE_TYPE* buffer, int samplecount);

	float* temp_buffer; // for easy debug dumping
	int buffer_width, buffer_height;

	protected:
	int frames_rendered; // how many stereo frames we have rendered (the timer)

	private:
	Synth(const Synth&); // disallow copy constructor
	Synth& operator=(const Synth&); // disallow assignment
	int serializeEvents(synth::eventbuffer_t& events); // Returns the amount of events serialized.

	std::unique_ptr<FBO> framebuffer;
	eng::fsys::File shadersource;
	ShaderProgram shader;

	GLuint eventListUBO;
	uint8_t* eventListData;
};