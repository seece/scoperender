/* Steinberg ASIO audio back end. */
#pragma once

typedef void (*audio_asio_callback_t)(float* left, float* right, int samples);
void audio_asio_init(void* hwnd, double samplerate);
void audio_asio_start();
//void audio_asio_stop();
void audio_asio_set_callback(audio_asio_callback_t callback);
// asio callback with two channels
// if callback is to synth interface it will need a this pointer
void audio_asio_kill();