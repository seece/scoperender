#pragma once
#include <cstdint>
#include <vector>

namespace synth {

enum EventType {
	EVENT_NONE,
	EVENT_NOTE_ON,
	EVENT_NOTE_OFF
};

class Event {
	public:
	int begin;	
	EventType type;
	float data;	// basic message data
	float param; // extra parameter
};

typedef std::vector<Event> eventbuffer_t;

}