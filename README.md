# Oscilloscope line renderer

This program receives 2d line data via a named pipe, and renders an audio signal that shows the lines when inspected with an oscilloscope set in XY-mode. See [the techinal report](http://www.lofibucket.com/articles/oscilloscope_quake.html) for details.

For line input use the modified Quake engine found from the [darkplaceswire](https://bitbucket.org/seece/darkplaceswire) project.

**Note: This repository doesn't seem to include all files to successfully compile the PortAudio backend. So be prepared to fix it on your own if you want it to run!** Sorry about that.

## Building

1. Download [ASIO SDK 2.3][asiosdk] and extract it to `asiosdk/` directory. Because of license restrictions this can't be distributed with the repository.
2. Build `portaudio\build\msvc\portaudio.sln` with the `ReleaseMinDependency` config.
3. Copy `portaudio\build\msvc\Win32\ReleaseMinDependency\portaudio_x86.dll` to `gpusyn/`
3. SDL2 is already built, its DLL is stored in `gpusyn/SDL2.dll`
4. glload is already built, its static libraries are at `glsdk_0_5_2\glload\lib`
5. Finally, build and run `gpusyn/gpusyn.sln`

The `gpusyn` project is configured to read the static libraries from the correct repository subdirectories.

## Usage
The program plays audio that matches the input lines. An OpenGL visualisation is shown on screen, if enabled. To check the validity of the audio output, record a segment of the signal to a file and view it with the [XY-scope simulator](https://gist.github.com/seece/034e8c3c9debb0464d3a).


Keyboard controls:

* **Up/Down**: Increase/decrease line draw speed
* **S/A**: Increase/decrease line burst size
* **X/Z**: Increase/decrease buffer fill threshold
* **R**: Toggle recording
* **V**: Toggle visuals

## Notes
* The communication between the two programs is quite buggy and usually deadlocks after a while.
* I might have forgotten some dependency.
* An ASIO audio driver is required.
* The system is hardcoded to pick the first ASIO4ALL device.

## License
All code under `gpusyn/` is MIT licensed, see `COPYING` for details. Other projects are under their respective original licenses.

[asiosdk]: http://www.steinberg.net/sdk_downloads/asiosdk2.3.zip

