For some reason Portaudio requires ksguid.lib even though I'm explicitly defining NOT to use that API.

Find all "ksguid.lib", Subfolders, Find Results 1, Entire Solution, ""
  C:\dev\gpusyn\portaudio\src\os\win\pa_win_wdmks_utils.c(71):        #pragma comment( lib, "ksguid.lib" )
  Matching lines: 1    Matching files: 1    Total files searched: 33

I removed this pragma but the error persisted.

Then I proceeded to remove all WDMKS related files from the project, but still:
	Error	5	error LNK1104: cannot open file 'ksguid.lib'	C:\dev\gpusyn\portaudio\build\msvc\LINK	portaudio

After grepping the directory I realised it was defined as an additional dependency in portaudio.vcxproj. I removed it from the dependencies with a text editor and the build succeeded.

I also removed USE_WDMKS_DEVICE_INFO flag, or something along those lines.


## Unnecessary 
The line

	PaError PaWinWdm_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex index );
	
from pa_win_hostapis.c was also removed.
